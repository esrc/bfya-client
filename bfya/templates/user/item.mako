<%inherit file="../base.mako" />

<div id="home" data-role="page">

    ${parent.header()}

    <div data-role="content">

        <div data-role="fieldcontain">
            <ul data-role="listview" data-inset="true">
                <li><a href="/observe/group">Observe Group</a></li>
                <li><a href="/observe/individual">Observe Individuals</a></li>
            </ul>
        </div>
        <br/>

        <h2>Nearby Organizations</h2>

        Use geolocation service to find the closest five organizations and then list them here.

        <div data-role="fieldcontain">
            <label for="chart" class="select">Aggregate History of Relationships</label>
            <div id="chart" style="text-align: center;padding-top:20px;"><img src="/static/img/aggregate.png"></div>
        </div>

    </div><!-- /content -->

    ${parent.footer()}

</div><!-- /page -->
