"""
This file is subject to the terms and conditions defined in the
LICENSE file, which is part of this source code package.
"""

__description__ = "Organization views"

from pyramid.httpexceptions import HTTPNotFound
from pyramid.view import view_config
import bfya.models as Models
import logging

logger = logging.getLogger()

#------------------------------------------------------------------------------

@view_config(route_name='organization.index', renderer='organization/index.mako', request_method="GET")
def render_index(request):
    """
    Render the index page.
    """
    orgs = Models.Organization.all()
    for instance in orgs:
        print instance
    return {
        'organizations': orgs
    }

@view_config(route_name='organization.item', renderer='organization/item.mako')
def render_item(request):
    """
    Render the item.
    """
    alias = request.matchdict['id']
    org = Models.Organization.by_alias(alias)
    if not org:
        msg = "The specified organization record could not be found"
        logger.warning(msg)
        return HTTPNotFound()
    return {
        "organization": org
    }
