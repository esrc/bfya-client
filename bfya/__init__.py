"""
This file is subject to the terms and conditions defined in the
LICENSE file, which is part of this source code package.
"""

from bfya.models import initialize_sql
from pyramid_beaker import session_factory_from_settings
from pyramid.config import Configurator
from sqlalchemy import engine_from_config

import logging

logger = logging.getLogger()


#------------------------------------------------------------------------------


def configure_logging(LogFilePath):
    """Configure application logging.
    """
    logger.setLevel(logging.INFO)
    formatter = logging.Formatter('%(asctime)s -- %(levelname)s -- %(message)s')
    try:
        fh = logging.FileHandler(LogFilePath)
        fh.setFormatter(formatter)
        logger.addHandler(fh)
    except:
        msg = "Could not open log file {0} for writing".format(LogFilePath)
        logger.warning(msg)


def load_development_data():
    """
    Load development data set.
    """
    pass


def load_production_data():
    """
    Load production data set.
    """
    pass


def main(global_config, **settings):
    """ Create a Pyramid WSGI application.
    """
    settings['log_path'] = '/var/log/bfya.log'
    config = Configurator(settings=settings)
    session_factory = session_factory_from_settings(settings)
    # configure logging
    configure_logging(settings['log_path'])
    # configure database
    config.scan('bfya.models')
    engine = engine_from_config(settings, 'sqlalchemy.')
    initialize_sql(engine)
    # add routes
    config.add_static_view('static', 'static', cache_max_age=3600)
    config.add_route('home', '/')
    config.add_route('observation.index', '/observations')
    config.add_route('observation.group', '/observe/group')
    config.add_route('observation.individual', '/observe/individual')
    config.add_route('organization.index', '/organizations')
    config.add_route('organization.item', '/organization/{id}')
    config.add_route('user.index', '/users')
    config.add_route('user.item', '/user/{id}')
    # scan for views
    config.scan()
    # return application
    return config.make_wsgi_app()
