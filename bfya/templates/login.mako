<%inherit file="base.mako" />

<div id="home" data-role="page">

    ${parent.header()}

    <div data-role="content">

        <h1>Login</h1>
        <form action="/user/login" method="post">
            <input type="text" name="username" placeholder="User name" required />
            <input type="password" name="password" placeholder="Password" required />
            <button>Login</button>
        </form>

    </div><!-- /content -->

    ${parent.footer()}

</div><!-- /page -->
