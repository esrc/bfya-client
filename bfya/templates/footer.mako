<%block name="footer">
<div data-role="footer" data-id="home-footer" data-position="fixed">
    <div data-role="navbar">
        <ul>
            <li><a href="/" class="ui-btn-active ui-state-persist" data-icon="home">Home</a></li>
            <li><a href="/organizations" data-icon="grid">Organizations</a></li>
        </ul>
    </div>
</div><!-- /footer -->
</%block>
