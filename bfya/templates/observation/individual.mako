<%inherit file="../base.mako" />

<div id="observe-individual" data-role="page">

    ${parent.header()}

    <div data-role="content">
        <form method="post" action="/observe/individual">

            <div data-role="fieldcontain">
                <label for="observe-group-name" class="select">Organization</label>
                <select name="observe-group-name" id="observe-group-name">
                    <option value=""></option>
                    % for key in organizations:
                    <option value="${key}">${organizations[key]}</option>
                    % endfor
                </select>
            </div>

            <fieldset data-role="controlgroup">
                <legend>Party A</legend>
                % for key in party_a:
                <input type="radio" name="partya" id="partya-${key}" value="${key}">
                <label for="partya-${key}">${party_a[key]}</label>
                % endfor
            </fieldset>

            <div data-role="fieldcontain">
                <label for="partyb" class="select">Party B</label>
                <select name="partyb" id="partyb">
                    <option value=""></option>
                    % for key in party_b:
                    <option value="${key}">${party_b[key]}</option>
                    % endfor
                </select>
            </div>

            <div data-role="fieldcontain">
                <label for="slider-fill">Quality of Relationship</label>
                <input type="range" name="quality" id="quality" value="2" min="1" max="3" step="1" data-highlight="true">
            </div>

            <div data-role="fieldcontain">
                <label for="comments">Comments:</label>
                <textarea cols="40" rows="10" name="comments" id="comments"></textarea>
            </div>

            <input type="submit" value="Save" data-theme="b" />
        </form>

    </div><!-- /content -->

    ${parent.footer()}

</div><!-- /page -->
