"""
This file is subject to the terms and conditions defined in the
LICENSE file, which is part of this source code package.
"""

__description__ = "Models"

from sqlalchemy import Boolean, Column, Integer, UnicodeText, Unicode, VARCHAR
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import (
    scoped_session,
    sessionmaker,
    )
import sqlalchemy as sa
import hashlib


DBSession = scoped_session(sessionmaker())
Base = declarative_base()

def initialize_sql(engine):
    DBSession.configure(bind=engine)
    Base.metadata.bind = engine
    Base.metadata.create_all(engine)

#------------------------------------------------------------------------------
# Model Classes

class Group(Base):
    """
    A social group that is being measured within the body of subject
    organizations.
    """
    __tablename__ = 'Group'
    id = Column(Integer, primary_key=True)
    alias = Column(Unicode(255), unique=True)
    name = Column(Unicode(255), unique=True)

    @classmethod
    def all(cls):
        return DBSession.query(Group).order_by(sa.desc(Group.name))

    @classmethod
    def by_id(cls, Id):
        return DBSession.query(Group).filter(Group.id == Id).first()

    @classmethod
    def by_name(cls, Name):
        return DBSession.query(Group).filter(Group.name == Name).first()


class GroupObservation(Base):
    """
    An observation made by a single observer of a named group.
    """
    __tablename__ = 'groupObservation'
    id = Column(Integer, primary_key=True)
    observer = Column(Unicode(255), unique=True)
    organization = Column(Unicode(255))
    partya = Column(Unicode(255))
    partyb = Column(Unicode(255))
    quality = Column(Integer)
    notes = Column(UnicodeText)


class Individual(Base):
    """
    An individual that is being measured within the body of subject
    organizations.
    """
    __tablename__ = 'individual'
    id = Column(Integer, primary_key=True)
    name = Column(Unicode(255), unique=True)

    @classmethod
    def all(cls):
        return DBSession.query(Individual).order_by(sa.desc(Individual.name))

    @classmethod
    def by_id(cls, Id):
        return DBSession.query(Individual).filter(Individual.id == Id).first()

    @classmethod
    def by_name(cls, Name):
        return DBSession.query(Individual).filter(Individual.name == Name).first()


class IndividualObservation(Base):
    """
    An observation made by a single observer of another individual.
    """
    __tablename__ = 'IndividualObservation'
    id = Column(Integer, primary_key=True)
    observer = Column(Unicode(255), unique=True)
    organization = Column(Unicode(255))
    partya = Column(Unicode(255))
    partyb = Column(Unicode(255))
    quality = Column(Integer)
    notes = Column(UnicodeText)


class Organization(Base):
    """
    An organization that is being measured.
    """
    __tablename__ = 'Organization'
    id = Column(Integer, primary_key=True)
    alias = Column(Unicode(255))
    name = Column(Unicode(255), unique=True)
    alternate_names = Column(Unicode(255))
    location = Column(Unicode(255))
    latitude = Column(Unicode(64))
    longitude = Column(Unicode(64))
    fromdate = Column(Unicode(128))
    todate = Column(Unicode(128))
    url = Column(Unicode(255))
    summary = Column(UnicodeText)

    def __init__(self, Name, Alias, Alternate_Names):
        self.name = Name
        self.alias = Alias

    @classmethod
    def all(cls):
        return DBSession.query(Organization).order_by(sa.desc(Organization.name))

    @classmethod
    def by_id(cls, Id):
        return DBSession.query(Organization).filter(Organization.id == Id).first()

    @classmethod
    def by_alias(cls, Alias):
        return DBSession.query(Organization).filter(Organization.alias == Alias).first()

    @classmethod
    def by_name(cls, Name):
        return DBSession.query(Organization).filter(Organization.name == Name).first()


class User(Base):
    """
    User account. Accounts are of type user or administrator. Administrator
    accounts should reveal additional functionality in the interface, to
    enable maintenance of accounts and data.
    """
    __tablename__ = 'User'
    id = Column(Integer, primary_key=True)
    name = Column(Unicode(255), unique=True)
    password_hash = Column(VARCHAR)
    admin = Column(Boolean)

    def __init__(self, Name, Password, Admin=False):
        self.name = Name
        h = hashlib.sha512()
        h.update(Password)
        self.password_hash = h.hexdigest()
        self.admin = Admin

    @classmethod
    def all(cls):
        return DBSession.query(User).order_by(sa.desc(User.name))

    @classmethod
    def by_id(cls, Id):
        return DBSession.query(User).filter(User.id == Id).first()

    @classmethod
    def by_name(cls, Name):
        return DBSession.query(User).filter(User.name == Name).first()
