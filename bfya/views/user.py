"""
This file is subject to the terms and conditions defined in the
LICENSE file, which is part of this source code package.
"""

__description__ = "User views"

from pyramid.httpexceptions import HTTPNotFound
from pyramid.view import view_config
import bfya.models as Models
import logging

logger = logging.getLogger()

#------------------------------------------------------------------------------

@view_config(route_name='home', renderer='login.mako')
def render_home(request):
    """
    Render the home view.
    """
    return {}

@view_config(route_name='user.index', renderer='user/index.mako')
def render_index(request):
    """
    Render the user page.
    """
    users = Models.User.all()
    return {
        "users": users
    }

@view_config(route_name='user.item', renderer='user/item.mako')
def render_item(request):
    """
    Render the user page.
    """
    user_id = request.matchdict['id']
    user = Models.User.by_id(user_id)
    if not user:
        msg = "The specified user record could not be found"
        logger.warning(msg)
        return HTTPNotFound()
    logger.debug("Render user {0}".format(user_id))
    return {
        "user": user
    }
