"""
This file is subject to the terms and conditions defined in the
LICENSE file, which is part of this source code package.
"""

__description__ = "Organization views"

from pyramid.httpexceptions import HTTPFound
from pyramid.view import view_config
import bfya.models as Models
import logging


logger = logging.getLogger()

#------------------------------------------------------------------------------

@view_config(route_name='observation.group', request_method="POST")
def handle_observe_group(request):
    """
    Handle new observation.
    """
    # user = request.matchdict['user']
    org = request.POST['organization']
    party_a = request.POST['party_a']
    party_b = request.POST['party_b']
    quality = request.POST['quality']
    notes = request.POST['notes']
    # redirect to the user's start page
    url = '/user/login'
    return HTTPFound(url)

@view_config(route_name='observation.individual', request_method="POST")
def handle_observe_individual(request):
    """
    Handle new observation.
    """
    pass

@view_config(route_name='observation.index', renderer='observation/index.mako', request_method="GET")
def render_index(request):
    """
    Render the anonymized list of recent observations.
    """
    group_observations = Models.GroupObservation.all()
    individual_observations = Models.IndividualObservation.all()
    return {
        "group":group_observations,
        "individual": individual_observations,
    }

@view_config(route_name='observation.group', renderer='observation/group.mako', request_method="GET")
def render_observe_group(request):
    """
    Render the observation data entry form.
    """
    party_a = {
        "artesan-teachers":"Artesan Teachers",
        "hol-students":"HOL Students",
    }
    return {
        'party_a' : party_a,
        'party_b' : Models.getGroups(),
        'organizations': Models.getOrganizations()
    }

@view_config(route_name='observation.individual', renderer='observation/individual.mako')
def render_observe_individual(request):
    """
    Render the observation data entry form.
    """
    return {
        'party_a' : Models.getIndividuals(),
        'party_b' : Models.getIndividuals(),
        'organizations' : Models.getOrganizations()
    }

