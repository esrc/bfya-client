"""
This file is subject to the terms and conditions defined in the
LICENSE file, which is part of this source code package.
"""

from pyramid.paster import (
    get_appsettings,
    setup_logging,
    )
from sqlalchemy import engine_from_config
import bfya.models as Models
import bfya.scripts as Scripts
import logging
import inspect
import os
import sys
import transaction
import yaml

logger = logging.getLogger()


#------------------------------------------------------------------------------

def load_groups(Session, Path):
    """
    Load groups.
    """
    try:
        f = open(Path + os.sep + 'groups.yml')
        records = yaml.load(f)
        f.close()
        for record in records:
            item = Models.Group()
            for key in record:
                try:
                    setattr(item, key, record[key])
                except:
                    pass
            Session.add(item)
            logger.info("Added group")
    except:
        pass

def load_individuals(Session, Path):
    """
    Load individuals.
    """
    try:
        f = open(Path + os.sep + 'individuals.yml')
        records = yaml.load(f)
        f.close()
        for record in records:
            item = Models.Individual()
            for key in record:
                try:
                    setattr(item, key, record[key])
                except:
                    pass
            Session.add(item)
            logger.info("Added individual")
    except:
        pass

def load_organizations(Session, Path):
    """
    Load organizations
    """
    try:
        path = Path + os.sep + 'organizations.yml'
        f = open(path)
        records = yaml.load(f)
        f.close()
        for record in records:
            item = Models.Organization(record['name'], record['alias'])
            for key in record:
                try:
                    setattr(item, key, record[key])
                except:
                    pass
            Session.add(item)
            logger.info("Added organization")
    except:
        msg = "Could not complete import of organizations"
        logger.error(msg, exc_info=True)

def main(argv=sys.argv):
    """
    Initialize the database.
    """
    if len(argv) != 2:
        usage(argv)
    config_uri = argv[1]
    setup_logging(config_uri)
    settings = get_appsettings(config_uri)
    here = os.path.dirname(inspect.getfile(Scripts))
    engine = engine_from_config(settings, 'sqlalchemy.')
    Models.DBSession.configure(bind=engine)
    session = Models.DBSession()
    Models.Base.metadata.create_all(engine)
    with transaction.manager:
        load_groups(session, here)
        load_individuals(session, here)
        load_organizations(session, here)
    Models.DBSession.flush()

def usage(argv):
    """
    Show usage instructions.
    """
    cmd = os.path.basename(argv[0])
    print('usage: %s <config_uri>\n'
          '(example: "%s development.ini")' % (cmd, cmd))
    sys.exit(1)

if __name__ == '__main__':
    main(sys.argv)
