<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Building Futures for Young Australians</title>
    <!-- stylesheets -->
    <link rel="stylesheet" href="/static/css/jquerymobile/jquery.mobile-1.3.1.min.css">
    <link rel="stylesheet" href="/static/css/rickshaw/rickshaw.min.css">
    <!-- scripts -->
    <script src="/static/js/jquery/jquery-1.9.1.min.js"></script>
    <script src="/static/js/jquerymobile/jquery.mobile-1.3.1.min.js"></script>
    <script src="/static/js/d3js/d3.v3.min.js"></script>
    <%block name="head_scripts">
    </%block>
</head>
<body>

<%block name="header">
<!-- header -->
<div data-role="header" data-position="fixed">
    <h1>Building Futures for Young Australians</h1>
</div>
<!-- /header -->
</%block>

 ${self.body()}

<%block name="footer">
<!-- footer -->
<div data-role="footer" data-id="home-footer" data-position="fixed">
    <div data-role="navbar">
        <ul>
            <li><a href="/" class="ui-btn-active ui-state-persist" data-icon="home">Home</a></li>
            <li><a href="/organizations" data-icon="grid">Organizations</a></li>
            <li><a href="/observations" data-icon="grid">Observations</a></li>
        </ul>
    </div>
</div>
<!-- /footer -->
</%block>

</body>
</html>
