<%inherit file="../base.mako" />

<div id="organization" data-role="page">

    <div data-role="header" data-position="fixed">
        <h1>Building Futures</h1>
    </div><!-- /header -->

    <div data-role="content">

        <h2>Bellarine Secondary College</h2>
        <p>
            Peninsula Dr  Drysdale VIC 3222<br/>
            (03) 5251 9000
        </p>

        <hr/>

        <div data-role="fieldcontain">
            <label for="organization-state-diagram">Current State</label>
            <div id="organization-state-diagram" style="text-align: center">
                <img src="img/network.png" align='center' />
            </div>
        </div>

        <br/>

        <div data-role="fieldcontain">
            <label for="table-custom-2">Observations</label>
            <table data-role="table" id="table-custom-2" data-mode="columntoggle" class="ui-body-d ui-shadow table-stripe ui-responsive" data-column-btn-theme="b" data-column-btn-text="Columns" data-column-popup-theme="a">
                <thead>
                <tr class="ui-bar-d">
                    <th data-priority="1">Date</th>
                    <th data-priority="2">Observer</th>
                    <th data-priority="2">A</th>
                    <th data-priority="2">B</th>
                    <th data-priority="1">Rating</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td>2013-12-01</td>
                    <td>Observer ID</td>
                    <td>HOL Students</td>
                    <td>Principal</td>
                    <td>60</td>
                </tr>
                <tr>
                    <td>2013-11-01</td>
                    <td>Observer ID</td>
                    <td>HOL Students</td>
                    <td>Principal</td>
                    <td>60</td>
                </tr>
                <tr>
                    <td>2013-10-01</td>
                    <td>Observer ID</td>
                    <td>HOL Students</td>
                    <td>Principal</td>
                    <td>60</td>
                </tr>
                <tr>
                    <td>2013-09-01</td>
                    <td>Observer ID</td>
                    <td>HOL Students</td>
                    <td>Principal</td>
                    <td>60</td>
                </tr>
                <tr>
                    <td>2013-08-01</td>
                    <td>Observer ID</td>
                    <td>HOL Students</td>
                    <td>Principal</td>
                    <td>60</td>
                </tr>
                <tr>
                    <td>2013-07-01</td>
                    <td>Observer ID</td>
                    <td>HOL Students</td>
                    <td>Principal</td>
                    <td>60</td>
                </tr>
                <tr>
                    <td>2013-06-01</td>
                    <td>Observer ID</td>
                    <td>HOL Students</td>
                    <td>Principal</td>
                    <td>60</td>
                </tr>
                <tr>
                    <td>2013-05-01</td>
                    <td>Observer ID</td>
                    <td>HOL Students</td>
                    <td>Principal</td>
                    <td>60</td>
                </tr>
                <tr>
                    <td>2013-04-01</td>
                    <td>Observer ID</td>
                    <td>HOL Students</td>
                    <td>Principal</td>
                    <td>60</td>
                </tr>
                <tr>
                    <td>2013-03-01</td>
                    <td>Observer ID</td>
                    <td>HOL Students</td>
                    <td>Principal</td>
                    <td>60</td>
                </tr>
                <tr>
                    <td>2013-02-01</td>
                    <td>Observer ID</td>
                    <td>HOL Students</td>
                    <td>Principal</td>
                    <td>60</td>
                </tr>
                <tr>
                    <td>2013-01-01</td>
                    <td>Observer ID</td>
                    <td>HOL Students</td>
                    <td>Principal</td>
                    <td>60</td>
                </tr>
                </tbody>
            </table>
        </div>
    </div><!-- /content -->

    ${parent.footer()}

</div><!-- /page -->
