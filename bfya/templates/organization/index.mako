<%inherit file="../base.mako" />

<div id="organizations" data-role="page">

    ${parent.header()}

    <div data-role="content">
        <ul data-role="listview" data-filter="true">
            % for key in organizations:
            <li><a href="/organization/${key}">${organizations[key]}</a></li>
            %endfor
        </ul>
    </div><!-- /content -->

    ${parent.footer()}

</div><!-- /page -->
