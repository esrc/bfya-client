<%inherit file="../base.mako" />

<div id="observe-group" data-role="page">

    ${parent.header()}

    <div data-role="content">
        <form method="post" action="/observe/group">

            <div data-role="fieldcontain">
                <label for="organization" class="select">Organization</label>
                <select name="organization" id="organization">
                    <option value=""></option>
                    % for key in organizations:
                    <option value="${key}">${organizations[key]}</option>
                    % endfor
                </select>
            </div>

            <fieldset data-role="controlgroup">
                <legend>Party A</legend>
                % for key in party_a:
                <input type="radio" name="party_a" id="party_a-${key}" value="${key}">
                <label for="party_a-${key}">${party_a[key]}</label>
                % endfor
            </fieldset>

            <div data-role="fieldcontain">
                <label for="party_b" class="select">Party B</label>
                <select name="party_b" id="party_b">
                    <option value=""></option>
                    % for key in party_b:
                    <option value="${key}">${party_b[key]}</option>
                    % endfor
                </select>
            </div>

            <div data-role="fieldcontain">
                <label for="quality">Quality of Relationship</label>
                <input type="range" name="quality" id="quality" value="60" min="0" max="100" step="20" data-highlight="true">
            </div>

            <div data-role="fieldcontain">
                <label for="textarea">Notes:</label>
                <textarea cols="40" rows="20" name="notes" id="textarea"></textarea>
            </div>

            <input type="submit" value="Save" data-theme="b" />
        </form>

    </div><!-- /content -->

    ${parent.footer()}

</div><!-- /page -->
